<?php

namespace App\Tests\Security;

use App\Security\ApiTokenAuthenticator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class ApiTokenAuthenticatorTest extends TestCase
{
    /**
     * @test
     */
    public function it_returns_false_for_register_api_endpoint(): void
    {
        $authenticator = new ApiTokenAuthenticator();

        $request = $this->createMock(Request::class);
        $request
            ->expects(self::once())
            ->method('getRequestUri')
            ->willReturn('/api/register');

        $this->assertFalse($authenticator->supports($request));
    }

    /**
     * @test
     */
    public function it_throws_an_exception_when_header_does_not_contain_api_token(): void
    {
        $authenticator = new ApiTokenAuthenticator();

        $request = $this->createMock(Request::class);
        $request
            ->expects(self::once())
            ->method('getRequestUri')
            ->willReturn('/api/forgot-password');

        $request->headers = $this->createMock(HeaderBag::class);
        $request->headers
            ->expects(self::once())
            ->method('has')
            ->with('X-AUTH-TOKEN')
            ->willReturn(false);

        $this->expectException(CustomUserMessageAuthenticationException::class);
        $this->expectExceptionMessage('No API token provided');

        $authenticator->supports($request);
    }

    /**
     * @test
     */
    public function it_returns_true_when_api_token_is_provided_in_the_header(): void
    {
        $authenticator = new ApiTokenAuthenticator();

        $request = $this->createMock(Request::class);
        $request
            ->expects(self::once())
            ->method('getRequestUri')
            ->willReturn('/api/forgot-password');

        $request->headers = $this->createMock(HeaderBag::class);
        $request->headers
            ->expects(self::once())
            ->method('has')
            ->with('X-AUTH-TOKEN')
            ->willReturn(true);

        $this->assertTrue($authenticator->supports($request));
    }

    /**
     * @test
     */
    public function it_throws_an_exception_on_authentication_when_no_api_token_is_provided_in_the_headers(): void
    {
        $authenticator = new ApiTokenAuthenticator();

        $request = $this->createMock(Request::class);
        $request->headers = $this->createMock(HeaderBag::class);
        $request->headers
            ->expects(self::once())
            ->method('get')
            ->with('X-AUTH-TOKEN')
            ->willReturn(null);

        $this->expectException(CustomUserMessageAuthenticationException::class);
        $this->expectExceptionMessage('No API token provided');

        $authenticator->authenticate($request);
    }

    /**
     * @test
     */
    public function it_returns_user_passport_on_authentication(): void
    {
        $apiToken = 'some random token';

        $authenticator = new ApiTokenAuthenticator();

        $request = $this->createMock(Request::class);
        $request->headers = $this->createMock(HeaderBag::class);
        $request->headers
            ->expects(self::once())
            ->method('get')
            ->with('X-AUTH-TOKEN')
            ->willReturn($apiToken);

        $passport = new SelfValidatingPassport(new UserBadge($apiToken));

        $this->assertEquals($passport, $authenticator->authenticate($request));
    }

    /**
     * @test
     */
    public function it_returns_null_on_authentication_success(): void
    {
        $authenticator = new ApiTokenAuthenticator();

        $request = $this->createMock(Request::class);
        $token = $this->createMock(TokenInterface::class);

        $this->assertNull($authenticator->onAuthenticationSuccess($request, $token, 'some firewall name'));
    }

    /**
     * @test
     */
    public function it_returns_unauthorised_on_authentication_failure(): void
    {
        $messageKey = 'An authentication exception occurred.';
        $messageData = [];

        $authenticator = new ApiTokenAuthenticator();

        $request = $this->createMock(Request::class);

        $exception = $this->createMock(AuthenticationException::class);
        $exception
            ->expects(self::once())
            ->method('getMessageKey')
            ->willReturn($messageKey);
        $exception
            ->expects(self::once())
            ->method('getMessageData')
            ->willReturn($messageData);

        $data = [
            'message' => strtr($messageKey, $messageData)
        ];

        $response = new JsonResponse($data, Response::HTTP_UNAUTHORIZED);

        $this->assertEquals($response, $authenticator->onAuthenticationFailure($request, $exception));
    }
}
