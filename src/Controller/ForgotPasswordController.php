<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ForgotPasswordController extends AbstractController
{
    #[Route("/api/forgot-password", name: "forgot_password", methods: "POST")]
    public function register(
        Request $request,
        UserPasswordHasherInterface $passwordHasher,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository
    ): JsonResponse {
        try {
            $email = $request->request->get('email');
            if ($email === null || $email === '') {
                throw new \Exception('Email cannot be empty');
            }

            $errors = $validator->validate($email, [new Email(['message' => 'Invalid email address'])]);
            if (count($errors) > 0) {
                $errorMessages = [];
                foreach ($errors as $error) {
                    $errorMessages[] = $error->getMessage();
                }
                return $this->json(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
            }

            $plainTextPassword = $request->request->get('new-password');
            if ($plainTextPassword === null || $plainTextPassword === '') {
                throw new \Exception('Password cannot be empty');
            }

            $user = $userRepository->findOneBy(['email' => $email]);
            if (!$user instanceof User) {
                throw new \Exception('Cannot find user with email ' . $email);
            }

            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $plainTextPassword
            );
            $user->setPassword($hashedPassword);

            $entityManager->flush();
        } catch (\Exception $exception) {
            return $this->json(['errors' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['Reset password successful']);
    }
}