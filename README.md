# Forgot password

## Setup

* Clone the repository
* Copy distributed files to put them in the right locations
* Install Symfony
* Install PHP 8.1
* Install mySQL
* Change the database URL to match the host, user, password, database, and port of mySQL configuration on your own machine
* Install composer

```bash
composer install
bin/console doctrine:migrations:migrate
symfony server:start
```

## Running the application

* Create a user by going to /api/register endpoint with email and password as the body of the request
* You should get a token from the registration endpoint
* Copy that token
* Paste the token and put it in the header with key `X-AUTH-TOKEN` for /api/forgot-password endpoint with email and new-password as the body of the request

BONUS
* Unit test for the authenticator

```bash
bin/phpunit
```
